package chapter06;

public class chap66 {
	static double getDistance(int x, int y, int x1, int y1) {
		int xloc = x - x1;
		int yloc = y - y1;
		
		//Math.pow 에 대한 함수 기억하기! -> 승수 구해주는 함수
		return Math.sqrt(Math.pow(xloc, 2) + Math.pow(yloc, 2));
		//두 점 사이의 거리는 x좌표 간의 차이에 대한 제곱 수와 y좌표 간 차이에 대한 제곱 수를 더하여 루트를 수행한 값
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getDistance(1,1,2,2));
	}
}
