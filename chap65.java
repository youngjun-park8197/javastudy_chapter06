package chapter06;

class Student {
	String name;
	int ban;
	int no;
	int kor;
	int eng;
	int math;
	
//	Student(String name, int ban, int no, int kor, int eng, int math) {
//		this.name = name;
//		this.ban = ban;
//		this.no = no;
//		this.kor = kor;
//		this.eng = eng;
//		this.math = math;
//	}
	
	int getTotal() { // 메소드 선언부
		return kor + eng + math;
	}
	
	float getAverage() { //메소드 선언부
		return (float)getTotal()/3.0f;
	}
	
	String info() {
		return name + "," + ban + "," + no + "," + kor + "," + eng + "," + math;
	}	
}

public class chap65 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.name = "홍길동";
		student.ban = 1;
		student.no = 1;
		student.kor = 100;
		student.eng = 60;
		student.math = 76;
		
//		student = new Student("홍길동", 1, 1, 100, 60 ,76);
		
		System.out.println("이름 : " + student.name);
		System.out.println("총점 : " + student.getTotal());
		System.out.println("평균 : " + student.getAverage());
		System.out.println(student.info());
	}
}


