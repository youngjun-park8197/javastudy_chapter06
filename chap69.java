package chapter06;

class Marine { 
	int x=0, y=0; 	// Marine의 위치좌표 Marine (x,y) 
	int hp = 60;	// 현재 체력 
	int weapon = 6; // 공격력
	int armor = 0; 	// 방어력
	 
	void weaponUp() { 
		weapon++; 
	} 
	
	void armorUp() { 
		armor++; 
	} 
	
	void move(int x, int y) { 
		this.x = x; 
		this.y = y;
	} 
}

public class chap69 {	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Marine marine = new Marine();
//		System.out.println("병사의 위치 : " + marine.move(4,5));
		System.out.println("병사 체력 : " + marine.hp);
		System.out.println("병사 공격력 : " + marine.weapon);
		System.out.println("병사 방어력 : " + marine.armor);
	}
}
